### LIS4381 fall 15 assignment 3
## Nick Powell


# Deliverables:

>Entity Relationship Diagram (ERD)  
>Include data (at least 10 records each table)  
>Provide Bitbucket read-only access to lis4381 repo (Language SQL), include all
files:  
a. a3.mwb  
b. a3.sql  
c. a3.png (export a3.mwb file as a3.png)  
d. README.md (MUST display a3.png ERD)  
>Links:
a. Lis4381 Bitbucket repo  
b. Lis4381 Web site (also displaying your a3.png) from your Web host

![A3 ERD Requirements](https://bitbucket.org/nick-powell/lis4381_fall15_a3/raw/master/images/a3.png "A3 ERD")